<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Document;
use AppBundle\Entity\Product;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ProductManager
{
    private $om;

    private $targetDir;

    public function __construct($rootDir, $targetDir, ObjectManager $om)
    {
        $this->targetDir = $rootDir . $targetDir;
        $this->om = $om;
    }

    private function uploadImage($file)
    {
        /** @var UploadedFile $file */
        $path = sha1(uniqid(mt_rand(), true)) . '.' . $file->guessExtension();
        $file->move($this->targetDir, $path);

        return $path;
    }

    public function processFiles(Product $product)
    {
        $uploadedFiles = $product->getUploadedFiles();
        foreach ($uploadedFiles as $file) {
            $path = $this->uploadImage($file);
            $document = new Document();
            $document->setName($path);
            $document->setProduct($product);
            $product->addDocument($document);

            unset($file);
        }
    }

    public function save(Product $product)
    {
        $this->om->persist($product);
        $this->om->flush();
    }
}