<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Document;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Product;
use AppBundle\Form\ProductType;

/**
 * Product controller.
 *
 * @Route("/product")
 */
class ProductController extends Controller
{
    /**
     * Lists all Product entities.
     *
     * @Route("/", name="product_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $products = $em->getRepository('AppBundle:Product')->findAll();

        return $this->render('product/index.html.twig', array(
            'products' => $products,
            'uploadsDirectory' => $this->container->getParameter('uploads_directory')
        ));
    }

    /**
     * Creates a new Product entity.
     *
     * @Route("/new", name="product_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $product = new Product();
        $form = $this->createForm('AppBundle\Form\ProductType', $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($product->getUploadedFiles()) {
                $this->get('app.product_manager')->processFiles($product);
            }
            $this->get('app.product_manager')->save($product);

            return $this->redirectToRoute('product_show', array('id' => $product->getId()));
        }

        return $this->render('product/new.html.twig', array(
            'product' => $product,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Product entity.
     *
     * @Route("/{id}", name="product_show")
     * @Method("GET")
     */
    public function showAction(Product $product)
    {
        $deleteForm = $this->createDeleteForm($product);

        return $this->render('product/show.html.twig', array(
            'product' => $product,
            'delete_form' => $deleteForm->createView(),
            'uploadsDirectory' => $this->container->getParameter('uploads_directory')
        ));
    }

    /**
     * Displays a form to edit an existing Product entity.
     *
     * @Route("/{id}/edit", name="product_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Product $product)
    {
        $deleteForm = $this->createDeleteForm($product);
        $editForm = $this->createForm('AppBundle\Form\ProductType', $product);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            if ($product->getUploadedFiles()[0] !== null) {
                $this->get('app.product_manager')->processFiles($product);
            }
            $this->get('app.product_manager')->save($product);

            return $this->redirectToRoute('product_edit', array('id' => $product->getId()));
        }

        return $this->render('product/edit.html.twig', array(
            'product' => $product,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'uploadsDirectory' => $this->container->getParameter('uploads_directory')
        ));
    }

    /**
     * Deletes a Product entity.
     *
     * @Route("/{id}", name="product_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Product $product)
    {
        $form = $this->createDeleteForm($product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($product);
            $em->flush();
        }

        return $this->redirectToRoute('product_index');
    }

    /**
     * Creates a form to delete a Product entity.
     *
     * @param Product $product The Product entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Product $product)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('product_delete', array('id' => $product->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Creates a new Translation.
     *
     * @Route("/ajax/delete/file", name="ajax_delete_file")
     * @Method({"GET"})
     */
    public function ajaxDeleteFileAction(Request $request)
    {
        $idFile = $request->query->get('idFile');

        if (isset($idFile)) {
            $repository = $this->getDoctrine()
                ->getRepository('AppBundle:Document');
            /**
             * @var Document $fileDb
             */
            $fileDb = $repository->findOneById($idFile);
            $nameFile = $fileDb->getName();
            $rootDir = $this->get('kernel')->getRootDir();
            $folderUpload = $this->container->getParameter('uploads_directory');
            $fileDir = realpath($rootDir . '/../').'/web/'.$folderUpload.$nameFile;
            $fs = new Filesystem();

            try {
                $fs->remove($fileDir );
                $em = $this->getDoctrine()->getEntityManager();
                $em->remove($fileDb);
                $em->flush();

                $result = [
                    'result' => true,
                ];
            } catch (\Exception $e) {
                $result = [
                    'result' => false,
                    'message' => 'Exception: '. $e->getMessage()
                ];
            }

        } else {
            $result = [
                'result' => false,
                'message' => 'id is missing'
            ];
        }

        return new JsonResponse($result);
    }
}
