/**
 * Created by kostya on 08.06.16.
 */
$.fn.deleteFile = function(method) {
    var defaults = {
        url: '',
        deleteButton: this.selector
    };

    var methods = {

        init: function(params){
            var options = $.extend({}, defaults, params);
            methods._initTranslating(options);

            return this;
        },

        _initTranslating: function (options){

            $(document).on('click', options.deleteButton , function(event){
                options['idFile'] = $(options.deleteButton).data('fileId');
                $.ajax({
                    url: options.url,
                    type: 'GET',
                    data: {idFile: options.idFile},
                    success: function (response) {
                        if (response.result) {
                            $('#' + options.idFile).remove();
                        } else {
                            alert('error :' + response.message);
                        }
                    },
                    error: function(xhr, status, error){
                        alert('Ошибка на сервере: ' + error);
                    }
                });

            })
        }
    };

    if (methods[method]) {
        return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
    } else if (typeof method === 'object' || !method) {
        return methods.init.apply(this, arguments);
    } else {
        $.error('Метод "' + method + '" не определен');
    }
};