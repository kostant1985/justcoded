<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Category;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadCategoryData implements FixtureInterface
{

    public function load(ObjectManager $manager)
    {
        for($i = 1; $i <= 5; $i++){
            /**
             * @var Category $category
             */
            $category = new Category();
            $category->setName('Category- '.$i);

            $manager->persist($category);
        }

        $manager->flush();
    }
}