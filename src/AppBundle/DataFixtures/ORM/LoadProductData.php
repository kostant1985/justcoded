<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Product;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadProductData implements FixtureInterface
{

    public function load(ObjectManager $manager)
    {
        for($i = 1; $i <= 5; $i++){
            /**
             * @var Product $product
             */
            $product = new Product();
            $product->setName('Product- '.$i);

            $manager->persist($product);
        }

        $manager->flush();
    }
}