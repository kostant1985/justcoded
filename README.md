justCoded
=========

A Symfony project


1 спроектировать базу данных для следующих сущностей:

- продукты с категориями.

- продукт может быть в разных категориях.

- категории с иерархической структурой ( id, parent_id)

- у продуктов может быть список фото.

- у продуктов могут быть скидки в зависимости от количества.

2 для всего кроме последнего пункта (он не обязательный) реализовать стандартные

(насколько это возможно) CRUD


3 реализовать REST API метод getProductDetails(product_id) - в результирующем объекте

должна быть вся информация о продукте, включая картинки, категории, скидки

использовать минимальный набор необходимых полей для сущностей. только ключевые

(связи, названия и т.п.)

Install

1) git clone https://kostant1985@bitbucket.org/kostant1985/justcoded.git

2) cd justсoded/ 

3) composer install (composer install --ignore-platform-reqs    если проблемы с версией php http://stackoverflow.com/questions/34970816/how-to-work-around-ubuntu-php-version-for-composer-update)

4) php app/console doctrine:database:create

5) php app/console doctrine:schema:update --force

6) php app/console doctrine:fixtures:load

7) php app/console server:run

Обратиться к api:

/api/products/{id_product}